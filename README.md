This repository contains a set of python tools around
[Batsim](https://github.com/oar-team/batsim) to simplify experiments.

This repository is deprecated by its Go counterpart
[batexpe](https://github.com/batsim/batexpe).
#!/usr/bin/env python3
"""Utility functions to manage directories."""
import os


def create_dir_if_not_exists(directory):
    """Python mkdir -p."""
    if not os.path.exists(directory):
        os.makedirs(directory)

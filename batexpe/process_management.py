#!/usr/bin/env python3

"""Utility functions to manage processes."""
import asyncio
import async_timeout
import logging
import os
import subprocess

if async_timeout.__version__ != '1.2.0':
    raise Exception('This module only works with async_timeout 1.2.0 '
                    '(got {})'.format(async_timeout.__version__))

async def await_with_timeout(future, timeout):
    """Await wrapper, which handles a timeout."""
    with async_timeout.timeout(timeout):
        await future
        return future


def kill_processes_and_all_descendants(proc_set,
                                       logger=logging.getLogger('batexpe')):
    """Kill a set of processes and all their children."""
    pids_to_kill = set()
    for proc in proc_set:
        cmd = "pstree {} -pal ".format(proc.pid) + \
              " | cut -d',' -f2 | cut -d' ' -f1 | cut -d')' -f1"
        logger.debug('Running command "{}"'.format(cmd))
        p = subprocess.run(cmd, shell=True,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)

        if p.returncode != 0:
            raise Exception('Command "{}" returned {}'.format(
                cmd, p.returncode))

        for pid in [int(pid_str) for pid_str in
                    p.stdout.decode('utf-8').replace('\n',
                                                     ' ').strip().split(' ')]:
            pids_to_kill.add(pid)

    if os.getpid() in pids_to_kill:
        raise Exception('Cannot commit suicide...')

    logger.error("Killing remaining processes {}".format(pids_to_kill))
    cmd = "kill -9 {}".format(' '.join([str(pid) for pid in pids_to_kill]))
    logger.debug('Running command "{}"'.format(cmd))
    p = subprocess.run(cmd, shell=True,
                       stdout=subprocess.PIPE,
                       stderr=subprocess.PIPE)

    if p.returncode != 0:
        raise Exception('Command "{}" returned {}'.format(
            cmd, p.returncode))


def display_process_output_on_error(process_name,
                                    stdout_file,
                                    stderr_file,
                                    max_lines=42,
                                    logger=logging.getLogger('batexpe')):
    """Display process output when on error."""
    if (stdout_file is None) and (stderr_file is None):
        logger.error("Cannot retrieve any log about the failed process")
        return

    for filename, fname in [(stdout_file.name, "stdout"),
                            (stderr_file.name, "stderr")]:
        if filename is not None:
            # Let's retrieve the file length (ugly.)
            cmd_wc = "wc -l {}".format(filename)
            logger.debug('Running command "{}"'.format(cmd_wc))
            p = subprocess.run(cmd_wc, shell=True, stdout=subprocess.PIPE)
            if p.returncode != 0:
                raise Exception('Command "{}" returned {}'.format(
                    cmd_wc, p.returncode))

            nb_lines = int(str(p.stdout.decode('utf-8')).split(' ')[0])

            if nb_lines > 0:
                # Let's read the whole file if it is small
                if nb_lines <= max_lines:
                    with open(filename, 'r') as f:
                        logger.error('{} {}:\n{}'.format(process_name, fname,
                                                         f.read()))
                # Let's read the first and last lines of the file
                else:
                    cmd_head = "head -n {} {}".format(max_lines // 2, filename)
                    cmd_tail = "tail -n {} {}".format(max_lines // 2, filename)

                    logger.debug('Running command "{}"'.format(cmd_head))
                    p_head = subprocess.run(cmd_head, shell=True,
                                            stdout=subprocess.PIPE)

                    logger.debug('Running command "{}"'.format(cmd_tail))
                    p_tail = subprocess.run(cmd_tail, shell=True,
                                            stdout=subprocess.PIPE)

                    if p_head.returncode != 0:
                        raise Exception('Command "{}" returned {}'.format(
                            cmd_head, p_head.returncode))

                    if p_tail.returncode != 0:
                        raise Exception('Command "{}" returned {}'.format(
                            cmd_tail, p_tail.returncode))

                    logger.error('{} {}:\n{}\n...\n...\n... '
                                 '(truncated... whole log in {})\n'
                                 '...\n...\n{}'.format(
                                     process_name,
                                     fname, str(p_head.stdout.decode(
                                         'utf-8')), filename,
                                     str(p_tail.stdout.decode('utf-8'))))

async def execute_command_inner(command,
                                stdout_file=None,
                                stderr_file=None,
                                timeout=None,
                                process_name=None,
                                where_to_append_proc=None,
                                logger=logging.getLogger('batexpe')):
    """Execute one command asynchronously."""
    logger.debug('Asynchronously running command "{}"'.format(command))
    create = asyncio.create_subprocess_shell(command,
                                             stdout=stdout_file,
                                             stderr=stderr_file)
    # Wait for the subprocess creation
    proc = await create

    if where_to_append_proc is not None:
        where_to_append_proc.add(proc)

    with async_timeout.timeout(timeout):
        # Wait for subprocess termination
        await proc.wait()

    return proc, process_name


def execute_batsim_alone(execution_parameters,
                         logger=logging.getLogger('robin')):
    """Execute Batsim and wait for its termination."""
    loop = asyncio.get_event_loop()
    proc_set = set()

    output_dir = execution_parameters['output-dir']
    batsim_stdout_file = open('{d}/batsim.stdout'.format(d=output_dir), 'w')
    batsim_stderr_file = open('{d}/batsim.stderr'.format(d=output_dir), 'w')
    simulation_timeout = execution_parameters['simulation-timeout']

    try:
        out_files = {'Batsim': (batsim_stdout_file, batsim_stderr_file)}

        logger.info("Running Batsim (timeout={})".format(simulation_timeout))
        proc, pname = loop.run_until_complete(execute_command_inner(
            command=execution_parameters['batcmd'],
            stdout_file=batsim_stdout_file, stderr_file=batsim_stderr_file,
            timeout=simulation_timeout, process_name='Batsim',
            where_to_append_proc=proc_set, logger=logger))
        proc_set.remove(proc)

        if proc.returncode == 0:
            logger.info("{} finished".format(pname))
            return True
        else:
            logger.error("{} finished (returncode={})".format(pname,
                                                              proc.returncode))
            display_process_output_on_error(pname, *out_files[pname],
                                            logger=logger)
            return False
    except asyncio.TimeoutError:
        logger.error('Simulation timeout reached!'.format(simulation_timeout))
    except Exception as e:
        logger.error('Exception caught ({}): {}'.format(type(e), e))
    except:
        logger.error("Another exception caught!")

    if len(proc_set) > 0:
        kill_processes_and_all_descendants(proc_set,
                                           logger=logger)

    return False


async def run_wait_any(batsim_command,
                       sched_command,
                       batsim_stdout_file,
                       batsim_stderr_file,
                       sched_stdout_file,
                       sched_stderr_file,
                       timeout,
                       where_to_append_proc,
                       logger=logging.getLogger('robin')):
    """Asynchronously waits for the termination of Batsim or Sched."""
    done, pending = await asyncio.wait([
        execute_command_inner(batsim_command,
                              batsim_stdout_file, batsim_stderr_file,
                              None, "Batsim", where_to_append_proc,
                              logger=logger),
        execute_command_inner(sched_command,
                              sched_stdout_file, sched_stderr_file,
                              None, "Sched", where_to_append_proc,
                              logger=logger)],
        return_when=asyncio.FIRST_COMPLETED)
    return done, pending


def execute_batsim_and_sched(execution_parameters,
                             logger=logging.getLogger('robin')):
    """Execute Batsim+Sched and wait for their termination."""
    logger.info('async_timeout: {}'.format(async_timeout))
    loop = asyncio.get_event_loop()
    proc_set = set()

    output_dir = execution_parameters['output-dir']
    batsim_stdout_file = open('{d}/batsim.stdout'.format(d=output_dir), 'w')
    batsim_stderr_file = open('{d}/batsim.stderr'.format(d=output_dir), 'w')
    sched_stdout_file = open('{d}/sched.stdout'.format(d=output_dir), 'w')
    sched_stderr_file = open('{d}/sched.stderr'.format(d=output_dir), 'w')

    try:
        out_files = {'Batsim': (batsim_stdout_file, batsim_stderr_file),
                     'Sched': (sched_stdout_file, sched_stderr_file)}
        logger.info("Running Batsim and Sched (timeout={})".format(
            execution_parameters['simulation-timeout']))
        done, pending = loop.run_until_complete(run_wait_any(
            batsim_command=execution_parameters['batcmd'],
            sched_command=execution_parameters['schedcmd'],
            batsim_stdout_file=batsim_stdout_file,
            batsim_stderr_file=batsim_stderr_file,
            sched_stdout_file=sched_stdout_file,
            sched_stderr_file=sched_stderr_file,
            timeout=execution_parameters['simulation-timeout'],
            where_to_append_proc=proc_set))
        finished_tuples = [finished.result() for finished in done]

        if len(finished_tuples) not in {1, 2}:
            raise Exception('Neither of the processes finished!')

        if (len(done) == 2):
            logger.info("Both processes finished at the same time!")
            all_ok = True
            for finished_tuple in finished_tuples:
                if finished_tuple[0].returncode != 0:
                    all_ok = False
                    logger.error("{} finished (returncode={})".format(
                        finished_tuple[1],
                        finished_tuple[0].returncode))
                    display_process_output_on_error(
                        finished_tuple[1], *out_files[finished_tuple[1]],
                        logger=logger)
                else:
                    logger.info("{} finished".format(finished_tuple[1]))
            return all_ok

        # One (and only one) process finished.
        finished_proc, finished_pname = finished_tuples[0]
        finished_ok = finished_proc.returncode == 0
        proc_set.remove(finished_proc)

        if finished_ok:
            logger.info("{} finished".format(finished_pname))
            logger.info("Waiting for clean termination... (timeout={})".format(
                execution_parameters['success-timeout']))

            pending_future = next(iter(pending))
            finished = loop.run_until_complete(await_with_timeout(
                pending_future, execution_parameters['success-timeout']))
            finished_proc, finished_pname = finished.result()
            proc_set.remove(finished_proc)

            if finished_proc.returncode == 0:
                logger.info("{} finished".format(finished_pname))
                return True
            else:
                logger.error("{} finished (returncode={})".format(
                    finished_pname, finished_proc.returncode))
                display_process_output_on_error(finished_pname,
                                                *out_files[finished_pname],
                                                logger=logger)
                return False
        else:
            logger.error("{} finished (returncode={})".format(
                finished_pname, finished_proc.returncode))
            display_process_output_on_error(finished_pname,
                                            *out_files[finished_pname],
                                            logger=logger)
    except asyncio.TimeoutError:
        logger.error("Timeout reached!")
        logger.debug('done={}'.format(done))
        logger.debug('pending={}'.format(pending))
    except Exception as e:
        logger.error('Exception caught ({}): {}'.format(type(e), e))
    except:
        logger.error("Another exception caught!")

    if len(proc_set) > 0:
        kill_processes_and_all_descendants(proc_set,
                                           logger=logger)
        logger.info('Killing done')

    return False

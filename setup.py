#!/usr/bin/env python3
"""Batexpe setup.py."""

from setuptools import setup

setup(
    name='batexpe',
    version='0.0.1',
    packages=['batexpe'],
    author="Millian Poquet",
    author_email="millian.poquet@gmail.com",
    description="Experiment tools around the Batsim simulator",
    long_description=open('README.md').read(),
    install_requires=["async-timeout == 1.2.0",
                      "coloredlogs >= 7.1",
                      "docopt >= 0.6.2",
                      "PyYAML >= 3.12"
                      ],
    include_package_data=False,
    url='https://gitlab.inria.fr/batsim/batexpe',
    classifiers=['License :: OSI Approved :: LGPL-3.0',
                 'Programming Language :: Python :: 3.6'],
    entry_points={
        'console_scripts': [
            'robin = batexpe.robin:main',
        ],
    },
    license="LGPL-3.0"
)

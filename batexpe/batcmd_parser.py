#!/usr/bin/env python3

"""Utility functions to parse batsim commands."""
import argparse
import logging
import shlex


class ArgumentParserError(Exception):
    """A custom Exception type when parsing with argparse."""

    pass


class ThrowingArgumentParser(argparse.ArgumentParser):
    """An ArgumentParser that throws exceptions on errors."""

    def error(self, message):
        """Callback function called when parsing errors arise."""
        raise ArgumentParserError(message)


def find_info_from_batsim_command(batsim_command,
                                  logger=logging.getLogger('batexpe')):
    """Get information from the Batsim command."""
    split_command = shlex.split(batsim_command)

    if len(split_command) < 1:
        raise Exception('Invalid Batsim command <batcmd>')

    if 'batsim' not in split_command[0]:
        logger.warning("Batsim does not seem to be executed directly. "
                       "This may cause this script failing at detecting what "
                       "Batsim's socket is.")

        # Let's assume Batsim's command is just prefixed.
        # Let's find the index at which Batsim's command appears
        found_index = -1

        for i in range(1, len(split_command)):
            if 'batsim' in split_command[i]:
                found_index = i
                break

        if found_index == -1:
            logger.warning("Could not find where Batsim command starts in the "
                           "user-given command '{}'.".format(batsim_command))
        else:
            split_command = split_command[found_index:]

    batparser = ThrowingArgumentParser(prog=split_command[0],
                                       description='Batsim command parser',
                                       add_help=False)

    batparser.add_argument("-p", "--platform", type=str)
    batparser.add_argument("-w", "--workload", type=str, nargs='+')
    batparser.add_argument("-W", "--workflow", type=str, nargs='+')
    batparser.add_argument("--WS", "--workflow-start", type=str, nargs='+')

    batparser.add_argument("-e", "--export", type=str, default="out")
    batparser.add_argument("-m", "--master-host", default="master_host")
    batparser.add_argument("-E", "--energy", action='store_true')

    batparser.add_argument('--config-file', type=str, default="None")
    batparser.add_argument("-s", "--socket-endpoint",
                           type=str, default="tcp://localhost:28000")
    batparser.add_argument("--redis-hostname", type=str, default="127.0.0.1")
    batparser.add_argument("--redis-port", type=int, default=6379)
    batparser.add_argument("--redis-prefix", type=str, default='default')

    batparser.add_argument("--enable-sg-process-tracing", action='store_true')
    batparser.add_argument("--disable-schedule-tracing", action='store_true')
    batparser.add_argument(
        "--disable-machine-state-tracing", action='store_true')

    batparser.add_argument("--mmax", type=int, default=0)
    batparser.add_argument("--mmax-workload", action='store_true')

    batparser.add_argument("-v", "--verbosity",
                           type=str, default="information")
    batparser.add_argument("-q", "--quiet", action='store_true')

    batparser.add_argument("--workflow-jobs-limit", type=int, default=0)
    batparser.add_argument(
        "--ignore-beyond-last-workflow", action='store_true')

    batparser.add_argument("--allow-time-sharing", action='store_true')
    batparser.add_argument("--batexec", action='store_true')
    batparser.add_argument("--pfs-host", type=str, default="pfs_host")

    batparser.add_argument("-h", "--help", action='store_true')
    batparser.add_argument("--version", action='store_true')

    try:
        batargs = batparser.parse_args(split_command[1:])
        is_batexec = False
        if batargs.batexec:
            is_batexec = True

        return (batargs.socket_endpoint,
                is_batexec,
                batargs.export)
    except ArgumentParserError as e:
        logger.error("Could not retrieve Batsim socket from <batcmd>. "
                     "Parsing error: '{}'".format(e))
        raise Exception('Could not retrieve Batsim socket from <batcmd>.')

#!/usr/bin/env python3

"""
Robin manages the execution of one Batsim simulation.

Usage:
  robin --output-dir=<dir>
        --batcmd=<batsim-command>
        [--schedcmd=<scheduler-command>]
        [--simulation-timeout=<time>]
        [--socket-timeout=<time>]
        [--success-timeout=<time>]
        [(--verbose | --quiet | --debug)]
  robin <description-file>
        [(--verbose | --quiet | --debug)]
  robin generate <description-file>
        [--output-dir=<dir>]
        [--batcmd=<batsim-command>]
        [--schedcmd=<scheduler-command>]
        [--simulation-timeout=<time>]
        [--socket-timeout=<time>]
        [--success-timeout=<time>]
  robin -h | --help
  robin --version


Timeout options:
  --simulation-timeout=<time>   Simulation timeout in seconds.
                                If this time is exceeded, the simulation is
                                stopped. Default value is one week.
                                [default: 604800]

  --socket-timeout=<time>       Socket timeout in seconds.
                                If the socket is still not usable after this
                                time, the script is stopped.
                                [default: 10]

  --success-timeout=<time>      The timeout for the second process to complete
                                once the first process has finished
                                successfully (returned 0).
                                [default: 600]

Verbosity options:
  --quiet                       Only print critical information.
  --verbose                     Print information. Default verbosity mode.
  --debug                       Print debug information.
"""

from docopt import docopt
import asyncio
import coloredlogs
import logging
import os
import re
import subprocess
import sys
import time
import yaml

from batexpe.batcmd_parser import find_info_from_batsim_command
from batexpe.process_management import execute_batsim_alone
from batexpe.process_management import execute_batsim_and_sched
from batexpe.dir_utils import create_dir_if_not_exists


def generate_description_file(args):
    """Handle the --generate subcommand."""
    content = {'output-dir': args['--output-dir'] or 'output-dir-unset',
               'batcmd': args['--batcmd'] or 'batcmd-unset',
               'schedcmd': args['--schedcmd'] or 'schedcmd-unset',
               'simulation-timeout': args['--simulation-timeout'],
               'socket-timeout': args['--socket-timeout'],
               'success-timeout': args['--success-timeout']
               }
    with open(args['<description-file>'], 'w') as out_file:
        yaml.dump(content, out_file, default_flow_style=False)


def read_description_file(args,
                          logger=logging.getLogger('robin')):
    """Retrieve the execution parameters from the <description-file>."""
    filename = args['<description-file>']
    parameters = dict()

    with open(filename, 'r', encoding='utf-8') as in_file:
        file_content = in_file.read()
        content = yaml.load(file_content)
        should_stop = False

        params_to_check = {'output-dir': str,
                           'batcmd': str,
                           'schedcmd': str,
                           'simulation-timeout': int,
                           'socket-timeout': int,
                           'success-timeout': int}

        # Check that all parameters are present and with the right type
        for (expected_param, expected_type) in params_to_check.items():
            if expected_param not in content:
                should_stop = True
                logger.error("Missing '{}' parameter".format(expected_param))
            else:
                value = content[expected_param]
                if not isinstance(value, expected_type) and value is not None:
                    should_stop = True
                    logger.error('Type of parameter "{}" is not "{}" '
                                 '(value="{}", type="{}")'.format(
                                     expected_param, expected_type,
                                     value, type(value)))
                else:
                    parameters[expected_param] = value
        if should_stop:
            raise Exception('Invalid <description-file> {}'.format(filename))

    return parameters


def read_command_line(args):
    """Retrieve the execution parameters from the command line."""
    params_to_read = {'output-dir',
                      'batcmd',
                      'schedcmd',
                      'simulation-timeout',
                      'socket-timeout',
                      'success-timeout'}
    parameters = {p: args['--' + p] for p in params_to_read}
    return parameters


def wait_for_batsim_socket_to_be_usable(sock='tcp://localhost:28000',
                                        timeout=5,
                                        seconds_to_sleep=0.1,
                                        logger=logging.getLogger('robin')):
    """Wait some time until the socket is available."""
    logger.info("Waiting for socket '{}' to be usable".format(sock))
    if timeout is None:
        timeout = 5
    remaining_time = timeout
    while remaining_time > 0 and socket_in_use(sock):
        time.sleep(seconds_to_sleep)
        remaining_time -= seconds_to_sleep
    return remaining_time > 0


def socket_in_use(sock,
                  logger=logging.getLogger('robin')):
    """Return whether the given socket is being used."""
    # Let's check whether the socket uses a port
    port_regex = re.compile('.*:(\d+)')
    m = port_regex.match(sock)
    if m:
        port = int(m.group(1))
        cmd = "ss -ln | grep ':{port}'".format(port=port)

        logger.debug('Running command "{}"'.format(cmd))
        p = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
        # grep fails when nothing is found

        if p.returncode == 0:
            return False
        else:
            return len(p.stdout.decode('utf-8')) > 0

    return False


def main():
    """Robin entry point."""
    # Parse command-line arguments
    args = docopt(__doc__, version="0.1")

    # Manage verbosity options
    verbosity_level = None
    if args['--quiet']:
        verbosity_level = logging.WARNING
    elif args['--debug']:
        print('Hello!')
        verbosity_level = logging.DEBUG
    else:
        verbosity_level = logging.INFO

    logger = logging.getLogger('robin')
    coloredlogs.install(milliseconds=True,
                        logger=logger,
                        level=verbosity_level)

    # Convert integer values to the right type
    for param in ['simulation-timeout', 'socket-timeout', 'success-timeout']:
        args['--' + param] = int(args['--' + param])

    # Manage the generate subcommand
    if args['generate']:
        generate_description_file(args)
        sys.exit(0)

    # Retrieve what should be executed, either from command-line or input file
    execution_parameters = None
    if args['<description-file>'] is not None:
        execution_parameters = read_description_file(args)
    else:
        execution_parameters = read_command_line(args)
    logger.info('Execution parameters: {}'.format(execution_parameters))

    # Prepare output logs
    create_dir_if_not_exists(execution_parameters['output-dir'])

    # Parse Batsim command
    (batsim_socket, is_batexec, export_prefix) = find_info_from_batsim_command(
        execution_parameters['batcmd'], logger)

    export_dir = os.path.dirname(export_prefix)
    output_dir = os.path.dirname(execution_parameters['output-dir'] + '/')
    if export_dir != output_dir:
        logger.warn('Batsim prefix ({}) mismatches <output-dir> ({})'.format(
            export_prefix, execution_parameters['output-dir']))

    # Execute the simulation
    if execution_parameters['schedcmd'] is None:
        # Batsim should be executed alone (without scheduler)
        if not is_batexec:
            logger.error('Batsim expects a scheduler but <schedcmd> is unset, '
                         'which is inconsistent')
            sys.exit(1)
        execute_batsim_alone(execution_parameters)
    else:
        # Batsim should be executed with a scheduler
        if is_batexec:
            logger.error('Batsim is launched in a no-scheduler setup but '
                         '<schedcmd> is set, which is inconsistent')
            sys.exit(1)

        # Wait for the socket to be available
        socket_usable = wait_for_batsim_socket_to_be_usable(
            sock=batsim_socket, timeout=execution_parameters['socket-timeout'])

        if not socket_usable:
            logger.error("Socket {} is still busy (after timeout={})".format(
                batsim_socket, execution_parameters['socket-timeout']))
            sys.exit(2)

        logger.info("Socket {sock} is now usable".format(sock=batsim_socket))
        try:
            execute_batsim_and_sched(execution_parameters)
            logger.info('Execution finished!')
        except asyncio.TimeoutError:
            logger.error("Timeout reached!")
        except Exception as e:
            logger.error('Exception caught ({}): {}'.format(type(e), e))
        except:
            logger.error("Another exception caught!")


if __name__ == '__main__':
    main()
